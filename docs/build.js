const Metalsmith = require('metalsmith');
const md = require('metalsmith-markdownit');
const layouts = require('metalsmith-layouts');
const ancestry = require('metalsmith-ancestry');
const relative = require('metalsmith-relative');

const filetree = require('./metalsmith-filetree');

Metalsmith(__dirname)
    .destination('../dist/docs')
    .use(md({
        html: true,
        typographer: true,
        linkify: true,
    }))
    .use(ancestry())
    .use(filetree())
    .use(relative())
    .use(layouts({
        default: 'layout.njk',
        pattern: '**/*.html',
        engineOptions: {
            autoescape: false
        }
    }))
    .build(err => {
        if (err) throw err
    });
