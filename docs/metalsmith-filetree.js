module.exports = (opts) => function (files, metalsmith, done) {
    setImmediate(done);
    /**
     * Adds `filetree` to metadata, available in layout.
     * [
     *      { title: 'Hjem', path: '/index.html' },
     *      { title: 'Komponenter', path: '/komponenter/index.html', members: [
     *          { title: 'Knapp', path: '/komponenter/knapp.html' }
     *          { title: 'Innfelt', path: '/komponenter/innfelt.html' }
     *      ]}
     * ]
     */
    let filenames = Object.keys(files);

    let mainPages = filenames.filter(isMainPage);

    let filetree = mainPages.reduce((tree, filename) => {
        let file = files[filename];
        let page = {
            title: file.title,
            path: file.ancestry.path,
            members: filenames.filter(isMemberTo(file)).map(memberFilename => {
                let member = files[memberFilename];
                return {
                    title: member.title,
                    path: member.ancestry.path,
                };
            }),
        }
        tree.push(page);
        return tree;
    }, []);

    filetree.sort(sortByIndexThenPath);
    metalsmith.metadata().filetree = filetree;

    function isMainPage (filename) {
        let file = files[filename];
        return hasTitle(file) && (!isInFolder(file) || isIndex(file));
    }

    function hasTitle (file) {
        return file.title !== undefined
    }

    function isInFolder (file) {
        return file.ancestry.parent !== null;
    }

    function isIndex (file) {
        return file.ancestry.basename === 'index.html';
    }

    function isMemberTo (index) {
        return (filename) => {
            let file = files[filename];
            return isInFolder(file) && file !== index && file.ancestry.members.includes(index);
        }
    }

    function sortByIndexThenPath (a, b) {
        let fileA = files[a.path];
        let fileB = files[b.path];
        if (!isInFolder(fileA) && isIndex(fileA)) {
            return -1;
        } else if (!isInFolder(fileB) && isIndex(fileB)) {
            return 1;
        } else if (a.path < b.path) {
            return -1;
        } else if (a.path > b.path) {
            return 1;
        }
        return 0;
    }
}