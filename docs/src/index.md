---
title: Hjem
---

Hei! Dette er Trondheim kommunes designsystem.

### En rask introduksjon

#### Markdown

Denne siden er skrevet i markdown. Eksempel på hvordan man kan skrive markdown finner du i dokumentet [eksempel på markdown](eksempel-pa-markdown.html).

Det finnes også en interaktiv guide på https://commonmark.org/help/tutorial/.

#### Lage en ny side

For å opprette en side, legg en fil, eksempelvis `filnavn.md`, i mappen `docs/src` med innholdet:

```md
---
title: Tittelen på siden
---

Innholdet på siden...
```


#### Lage flere sider med samme tema

1. Opprett en mappe for temaet under `docs/src`, eksempelvis `docs/src/komponenter`.
2. Opprett hovedsiden, `index.md`, under `docs/src/komponenter` med innholdet

  ```md
  ---
  title: Komponenter
  ---

  Vi bruker komponenter fra ...
  ```

3. Opprett undersider, for eksempel `knapp.md`, under `docs/src/komponenter` med innholdet

  ```md
  ---
  title: Knapp
  ---

  Knapp brukes for handlinger ...
  ```


#### Bruke angular komponenter
I markdown-filene kan du også skrive HTML, for eksempel er knappen under skrevet som `<button>Send</button>`:

<button>Send</button>

Hvis HTML-koden er et angular element fra `src/app`, vil elementet tegnes i websiden. `src/app` inneholder per nå et element som heter `<tk-crud>`, som vi kan bruke direkte:

<tk-crud></tk-crud>
