---
title: Komponenter
---

Fylles ut med informasjon som er felles for komponenter. For eksempel:

- Hvilket rammeverk vi bruker som utgangspunkt.
- Hvor inspirasjon er hentet.
- Hvordan man bruker en komponent.
- Hvordan man lager en komponent.
- ...