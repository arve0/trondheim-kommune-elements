---
title: Visuelt uttrykk
---

## Logo

Byvåpenet kan benyttes alene uten navnet Trondheim kommune, for eksempel i digitale kanaler og versjoner for små skjermflater, men det skal komme tydelig fram at kommunen er avsender. 

### Byvåpen

Selve byvåpenet kan gjengis i to forskjellige varianter, avhengig av bruk.
Svart/hvitt-versjonen av byvåpenet skal primært brukes ved produksjon av trykksaker og materiell i svart/hvitt. Fargeversjonen består av blå, svart og lys grå (eller sølv) på hvit bunn, og benyttes både på trykk og i digitale kanaler.
Byvåpenet kan unntaksvis brukes i negativ (hvit), for eksempel ved trykk på klær, men dette skal avklares med Kommunikasjonsenheten i hvert enkelt tilfelle. 

Logoen består av blå, grå (eller sølv) og svart. Det er ikke tillat å endre fargene i logoen. 

Logoen skal som hovedregel plasseres øverst til venstre på nettsider, slik som på Trondheim kommunes hjemmeside. Logoen skal følge innholdets venstremarg. 
Unntak: Plassmangel kan gjøre at vi ønsker å unnta kommunenavnet og bare presentere logo i toppfeltet. For eksempel skal kommunenavnet i desktopvisning, men kunne unnlate å ha det med i mobilskjermvisning.

[Nedlastbare varianter av byvåpenet](http://www.trondheim.kommune.no/logo)

## Farger

I fargepaletten er det 10 grunnfarger å velge mellom. 

Fargene kan mikses på tvers av hverandre for å skape variasjon. Dette forutsetter god harmoni mellom fargene, logoen og eventuelle andre elementer som brukes sammen.

## Tekst

PT Sans skal som hovedregel brukes i titler og brødtekst på nettsider

PT Serif kan brukes i redaksjonelle produkt (for eksempel nettmagasin, nettavis). 

