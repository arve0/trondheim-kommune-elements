import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TkCrudComponent } from './tk-crud.component';

describe('TkCrudComponent', () => {
  let component: TkCrudComponent;
  let fixture: ComponentFixture<TkCrudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TkCrudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TkCrudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
