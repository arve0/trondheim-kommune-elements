import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Injector } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { createCustomElement } from '@angular/elements';
// import { HttpClientModule } from '@angular/common/http';
import { TkCrudComponent } from './tk-crud/tk-crud.component';

@NgModule({
  declarations: [ TkCrudComponent ],
  imports: [
    BrowserModule,
    FormsModule,
    // HttpClientModule,
  ],
  providers: [],
  entryComponents: [ TkCrudComponent ],
})
export class AppModule {
  constructor (private injector: Injector) { }

  ngDoBootstrap () {
    const el = createCustomElement(TkCrudComponent, { injector: this.injector });
    customElements.define('tk-crud', el);
  }
}
